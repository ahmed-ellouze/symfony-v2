<?php

namespace App\Tests;


use App\Entity\Blogpost;
use PHPUnit\Framework\TestCase;
use DateTime;
class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost= new Blogpost();
        $datetime = new DateTime();
        $blogpost->setTitre('titre')
            ->setRceatedAt($datetime)
            ->setContenu('contenu')
            ->setSlug('slug');
        $this->assertTrue($blogpost->getTitre() === "titre");
        $this->assertTrue($blogpost->getRceatedAt() ===$datetime);
        $this->assertTrue($blogpost->getContenu() === "contenu");
        $this->assertTrue($blogpost->getSlug() === "slug");



    }

    public function testIsFalse()
    {
        $blogpost= new Blogpost();
        $datetime = new DateTime();
        $blogpost->setTitre('titre')
            ->setRceatedAt($datetime)
            ->setContenu('contenu')
            ->setSlug('slug');
        $this->assertFalse($blogpost->getTitre() === "false");
        $this->assertFalse($blogpost->getRceatedAt() === new DateTime());
        $this->assertFalse($blogpost->getContenu() === "false");
        $this->assertFalse($blogpost->getSlug() === "false");

    }

    public function testIsEmpty()
    {
        $blogpost= new Blogpost();
        $datetime = new DateTime();

        $this->assertEmpty($blogpost->getTitre());
        $this->assertEmpty($blogpost->getRceatedAt());
        $this->assertEmpty($blogpost->getContenu());
        $this->assertEmpty($blogpost->getSlug());

    }
}
